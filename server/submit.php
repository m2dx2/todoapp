<?php

	require("dbConnection.php");
	require("response.php");
	try {
		if (isset($_POST['task'])) {
			$task_data=(new db)->real_escape_string($_POST['task']);
			submit_task($task_data);
		}elseif (isset($_POST['task_id'])) {
			$id=(new db)->real_escape_string($_POST['task_id']);
			delete_task($id);
		}else{
			fetch_task();
		}
	} catch (Exception $e) {
		echo $e;
	}
	


	function fetch_task(){

		$query="call get_task()";
		$data = db::getInstance()->get_res($query,null);
		$status=sizeof($data)>0?1:0;
		echo response::make_response($data,$status);
	}
	function submit_task($data){

		$query='call submit_task("'.$data.'")';
		$id = db::getInstance()->get_res($query);
		if($id){
			echo response::make_response($id[0]['id'],1);
		}else{
			echo response::make_response([],0);
		}
		
	}
	function delete_task($id){
		$query='call delete_task("'.$id.'")';
		$id = db::getInstance()->get_res($query);
		//print_r($id);
		if($id){
			echo response::make_response($id[0]['status'],1);
		}else{
			echo response::make_response("No such task",0);
		}
	}
	//return ( json_encode($data));

?>