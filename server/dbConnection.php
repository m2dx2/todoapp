<?php
    include('config.php');
    class db extends mysqli {

        private static $instance = null;
        private $user = DBUSER;
        private $pass = DBPWD;
        private $dbName = DBNAME;
        private $dbHost = DBHOST;

        public static function getInstance() {
        if (!self::$instance instanceof self) {
                self::$instance = new self;
        }
            return self::$instance;
        }

        public function __construct() {
        parent::__construct($this->dbHost, $this->user, $this->pass, $this->dbName);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }
        parent::set_charset('utf-8');

       }
       public function dbquery($query)
        {
            if($this->query($query))
            {
                return 1;
            }else{
                return 0;
            }
            

        }
        public function get_res($query) 
        {
            $res = $this->query($query);
            $result=[];
            if ($res->num_rows > 0){
                while($row = $res->fetch_assoc()) {
                    array_push($result, $row);
                    
                }
                return $result;
            } else
            return null;
        }
    }


    ?>