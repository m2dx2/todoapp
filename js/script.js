	$("#submit").click(function(){
		text=$('#taskText').val();
		if(text.length>0){
			submitData(text);
			$('#taskText').val("");
		}else{
			alert("Task field is empty");
		}
	});

	function createListItem(data,id){
		item='<li class="list-group-item task" ><a id="task_'+id+'">'+data+'</a> <a id="'+id+'" type="button" class="btn btn-danger float-right remove"><span class="glyphicon glyphicon-search"></span> Remove </a></li>';
		return item;
	}
	function clearList(){
		$('#taskList').empty();
	}
	function populateList(text){
	    	//item=createListItem(text);
			$('#taskList').last().append(text);
	}
	$(document.body).on('click', '.remove' ,function(){
		id=$(this).attr("id");
		deleteTask(id);
		$(this).parent().remove();
	
	});
	$( 'form' ).bind('keypress', function(e){
	   if ( e.keyCode == 13 ) {
	   		//to stop default bootstrap validation and form submission
	   		e.preventDefault();
	     	$("#submit").click();
	   }
 	});
 	//store
 	function submitData(data){
	$.post( "server/submit.php", { task: data})
	  .done(function( res ) {
	  	res=JSON.parse(res);
	  	if(res.status==1){
	  		temp_item=createListItem(data,res.id);
	   		populateList(temp_item);
	  	}else{
	  		alert("Great!! You found a bug");
	  	}
	   	
	  });
 	}
 	//Fetch
 	var fetchData = new Promise(function(resolve, reject) {
 		var jqxhr = $.get( "server/submit.php", function() {

		})
		  .done(function(res) {
			res=JSON.parse(res);
		    resolve(res);
		  })
		  .fail(function() {
		     alert( "error while fetching" );
		  });
 	});

 	$( document ).ready(function() {
 		fetchData.then((res)=>{
 			list="";
 			if(res.status==1){
	 			$.each(res.data, function(k,v) {
	 				item=createListItem(v.description,v.id);
					list+=item;
				});
			    populateList(list);
 			}
 			
 		});
 		
	});
//delete task
function deleteTask(id){
	$.post( "server/submit.php", { task_id:id})
	  .done(function( res ) {

	  });
}
     var $loading = $('#loading').hide();
     //Attach the event handler to any element
     $(document)
       .ajaxStart(function () {
          //ajax request went so show the loading image
           $loading.show();
       })
     .ajaxStop(function () {
         //got response so hide the loading image
          $loading.hide();
      });
           //         <div id="loading">
           //      <img src="loading.gif" />  
           // </div>

